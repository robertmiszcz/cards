# -*- coding: utf-8 -*-

from django.forms import ModelForm
from . import models

class CardForm(ModelForm):

	class Meta:
		model = models.Card
		fields = ('nazwa','imie', 'nazwisko')
