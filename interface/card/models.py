from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

class Card(models.Model):
	nazwa = models.CharField(verbose_name='Card number', max_length=15)
	imie = models.CharField(max_length=15)
	nazwisko = models.CharField(max_length=20)
#	data = models.DateField('dodano', auto_now_add=True)
	autor = models.ForeignKey(User, on_delete=models.CASCADE)
# Create your models here.
