from django.conf.urls import url
from . import views

from django.contrib.auth.decorators import login_required
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from .models import Card

app_name = 'card'
urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^lista/', login_required(ListView.as_view(model=Card)),
		name='lista'),
	url(r'^dodaj/$',views.CardAdd.as_view(), name='dodaj'),
]

