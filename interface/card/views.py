# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from . import models
from . import forms
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse

def index(request):
	"""Strona główna"""
	kontekst = {'komunikat': 'Witaj w aplikacji Card!'}
#	return render(request, 'card/index.html', kontekst)
	return TemplateResponse(request, 'card/index.html', kontekst)
@method_decorator(login_required,'dispatch')
class CardAdd(CreateView):

	model = models.Card
	form_class = forms.CardForm
	success_url = reverse_lazy('card:lista')

	def get_context_data(self, **kwargs):
		context = super(CardAdd, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form = self.get_form()
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		form.instance.autor = self.request.user
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		return self.render_to_response(
			self.get_context_data(form=form)
		)
#Create your views here.
